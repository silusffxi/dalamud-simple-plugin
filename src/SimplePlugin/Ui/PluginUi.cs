using System;
using Dalamud.Interface.Windowing;
using SimplePlugin.Ui.Windows;

namespace SimplePlugin.Ui
{
    public class PluginUi : IDisposable
    {
        private WindowSystem Windows { get; } = new();

        private ConfigWindow WindowConfig { get; }

        private MainWindow WindowMain { get; }

        public PluginUi(ServiceContainer services)
        { 
            WindowMain = new MainWindow(services);
            WindowConfig = new ConfigWindow(services);

            Windows.AddWindow(WindowMain);
            Windows.AddWindow(WindowConfig);
        }

        ~PluginUi()
        {
            Dispose(false);
        }

        public void Draw()
        {
            Windows.Draw();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public void ShowMainWindow() => WindowMain.IsOpen = true;

        public void ShowConfigWindow() => WindowConfig.IsOpen = true;

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                Windows.RemoveAllWindows();
            }
        }
    }
}
