using Dalamud.Interface.Windowing;
using ImGuiNET;

namespace SimplePlugin.Ui.Windows
{
    public class ConfigWindow : Window
    {
        private ServiceContainer Services { get; }

        public ConfigWindow(ServiceContainer services)
            : base("Simple Plugin - Configuration")
        {
            Services = services;
        }

        public override void Draw()
        {
            ImGui.Text("Configuration options go here.");
        }
    }
}
