using Dalamud.Interface.Windowing;
using ImGuiNET;

namespace SimplePlugin.Ui.Windows
{
    public class MainWindow : Window
    {
        private ServiceContainer Services { get; }

        public MainWindow(ServiceContainer services)
            : base("Simple Plugin")
        {
            Services = services;
        }

        public override void Draw()
        {
            var player = Services.ClientState.LocalPlayer;
            if (player != null)
            {
                ImGui.Text($"Player Name: {player.Name}");
                ImGui.Text($"Home Server: {player.HomeWorld.GameData?.Name ?? ""}");
                ImGui.Text($"HP: {player.CurrentHp}/{player.MaxHp}");
                ImGui.Text($"MP: {player.CurrentMp}/{player.MaxMp}");
            }
            else
            {
                ImGui.Text("No active player!");
            }

            ImGui.Separator();

            var target = Services.TargetManager.Target;
            if (target != null)
            {
                ImGui.Text($"Target Name: {target.Name}");
                ImGui.Text($"Game Object ID: {target.GameObjectId}");
            }
            else
            {
                ImGui.Text("No Target");
            }
        }
    }
}
