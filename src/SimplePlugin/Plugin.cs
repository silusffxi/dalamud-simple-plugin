using System;
using Dalamud.Game.ClientState;
using Dalamud.Game.Command;
using Dalamud.Interface.Windowing;
using Dalamud.Plugin;
using Dalamud.Plugin.Services;
using SimplePlugin.Ui;

namespace SimplePlugin
{
    public sealed class Plugin : IDalamudPlugin
    {
        /// <summary>
        /// Hard coded, de facto name for the plugin.
        /// </summary>
        internal const string PLUGIN_NAME = "Simple Plugin";

        /// <summary>
        /// Command to invoke the plugin. This should be the command to show the configuration for the plugin if the plugin does not have a
        /// de facto "main" window.
        /// </summary>
        private const string PLUGIN_COMMAND = "/xlsimple";

        public bool Disposed { get; private set; }

        public string Name => PLUGIN_NAME;

        /// <summary>
        /// Allows plugin to setup its commands.
        /// </summary>
        private ICommandManager Commands { get; }

        /// <summary>
        /// This is, effectively a reference to Dalamud. Provides a way to call Dalamud functionality and
        /// interact with Dalamud data from the plugin.
        /// </summary>
        private IDalamudPluginInterface PluginInterface { get; }

        /// <summary>
        /// Contains services and components that should be made available to the plugin.
        /// </summary>
        private ServiceContainer Services { get; }

        /// <summary>
        /// Provides window management and UI for the plugin.
        /// </summary>
        private PluginUi Ui { get; }

        /// <summary>
        /// Plugin entry point.
        /// </summary>
        /// <param name="pluginInterface">Reference to Dalamud</param>
        /// <param name="cmdManager">Command manager to set up and manage commands for Dalamud</param>
        /// <param name="clientState"></param>
        /// <remarks>
        /// Entry point for the plugin. This will be called when the plugin in started by Dalamud.
        /// Anything that gets passed in through this constructor must be something that Dalamud can
        /// actually instantiate.
        /// </remarks>
        public Plugin(IDalamudPluginInterface pluginInterface, ICommandManager cmdManager)
        {
            Commands = cmdManager;
            PluginInterface = pluginInterface;

            // Use Dalamud's IoC to create the instance in order for the injection to
            // work correctly.
            Services = PluginInterface.Create<ServiceContainer>() ?? new ServiceContainer();

            Commands.AddHandler(PLUGIN_COMMAND, new CommandInfo(OnCommand)
            {
                HelpMessage = "Toggles the plugin's main window.",
                ShowInHelp = true,
            });

            // Create the plugin's UI objects.
            Ui = new PluginUi(Services);

            // Attach event handlers to be able to open the main and config window from the
            // Dalamud plugin installer window.
            PluginInterface.UiBuilder.OpenMainUi += Ui.ShowMainWindow;
            PluginInterface.UiBuilder.OpenConfigUi += Ui.ShowConfigWindow;
            PluginInterface.UiBuilder.Draw += Draw;
        }

        ~Plugin()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public void OnCommand(string cmd, string args) => Ui.ShowMainWindow();

        public void Draw()
        {
            Ui.Draw();
        }

        private void Dispose(bool disposing)
        {
            if (Disposed) return;

            if (disposing)
            {
                // Release any resources created directly by the plugin.

                // Release our command handlers.
                Commands.RemoveHandler(PLUGIN_COMMAND);
                PluginInterface.UiBuilder.Draw -= Draw;
                PluginInterface.UiBuilder.OpenMainUi -= Ui.ShowMainWindow;
                PluginInterface.UiBuilder.OpenConfigUi -= Ui.ShowConfigWindow;

                Ui.Dispose();
            }

            // Release any native resources, like pointers to native code,
            // or handles to external memory.

            Disposed = true;
        }
    }
}
