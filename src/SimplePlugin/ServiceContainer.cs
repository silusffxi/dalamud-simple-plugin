using Dalamud.Game.ClientState.Objects;
using Dalamud.IoC;
using Dalamud.Plugin.Services;

namespace SimplePlugin
{
    /// <summary>
    /// Contains objects, references, etc. to things that should be provided to components within the plugin.
    /// </summary>
    public class ServiceContainer
    {
        // Dalamud IoC will inject instances for these properties. This should ensure they
        // are assigned when the plugin is loaded.
#pragma warning disable CS8618
        [PluginService] public IChatGui Chat { get; private set; }

        [PluginService] public IClientState ClientState { get; private set; }

        [PluginService] public ICommandManager Commands { get; private set; }

        [PluginService] public ICondition Condition { get; private set; }

        [PluginService] public IDataManager DataManager { get; private set; }

        [PluginService] public IDutyState DutyState { get; private set; }

        [PluginService] public IGameGui GameGui { get; private set; }

        [PluginService] public IPluginLog Log { get; private set; }

        [PluginService] public ITargetManager TargetManager { get; private set; }

        [PluginService] public ITextureProvider TextureProvider { get; private set; }
#pragma warning restore CS8618
    }
}
